// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution(connect@ansory-solution.games)
// Licensed under BSD-2-Caluse
// File: package.js (ansory-solution/noname-selftest/package.js)
// Content:
// Copyright (c) 2022 Ansory Solution All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

extension["自用测试"] = {
	intro: "杂项技能合集，用于存储代码并提供测试途径",
	author: "Ansory Solution",
	netdisk: "",
	forum: "",
	version: null,
	files: ["extension.js"],
	size: "57KB"
};
