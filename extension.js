// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From Ansory Solution(connect@ansory-solution.games)
// Licensed under BSD-2-Caluse
// File: extension.js (ansory-solution/noname-selftest/extension.js)
// Content:
// Copyright (c) 2022 Ansory Solution All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

game.import("extension", (lib, game, ui, get, _ai, _status) => {
	// 定义变量
	const META = {
		name: "自用测试",
		branch: "Development",
		build: 1,
		year: "2022",
		month: "10",
		date: "30",
		times: "001",
	};

	return {
		name: META.name,
		editable: false,
		content(_config, _pack) {
			// 添加势力
			const GROUPS = {
				other: "另"
			};

			for (const NAME in GROUPS) {
				const item = GROUPS[NAME];
				let translate, translate2;
				if (Array.isArray(item)) {
					translate = item[0];
					translate2 = item[2] || translate;
					let style = document.createElement("style");
					let color1, color2, color3, color4;
					if (Array.isArray(item[1])) color1 = color2 = color3 = color4 = item[1];
					else ({ color1, color2, color3, color4 } = item[1]);
					style.innerHTML = [
						`.player.identity[data-color="${NAME}"],`,
						`div[data-nature="${NAME}"],`,
						`span[data-nature="${NAME}"] {text-shadow: black 0 0 1px,rgba(${color1.join()}) 0 0 2px,rgba(${color2.join()}) 0 0 5px,rgba(${color3.join()}) 0 0 10px,rgba(${color4.join()}) 0 0 10px}`,
						`div[data-nature="${NAME}m"],`,
						`span[data-nature="${NAME}m"] {text-shadow: black 0 0 1px,rgba(${color1.join()}) 0 0 2px,rgba(${color2.join()}) 0 0 5px,rgba(${color3.join()}) 0 0 5px,rgba(${color4.join()}) 0 0 5px,black 0 0 1px;}`,
						`div[data-nature="${NAME}mm"],`,
						`span[data-nature="${NAME}mm"] {text-shadow: black 0 0 1px,rgba(${color1.join()}) 0 0 2px,rgba(${color2.join()}) 0 0 2px,rgba(${color3.join()}) 0 0 2px,rgba(${color4.join()}) 0 0 2px,black 0 0 1px;}`
					].join("");
					document.head.appendChild(style);
				}
				else translate = translate2 = item;
				lib.group.add(NAME);
				lib.translate[NAME] = translate;
				lib.translate[`${NAME}2`] = translate2;
				lib.groupnature[NAME] = NAME;
			}

			// 增加后续
			const SKILLS = {
				"ansory_selftest_shield": "这只是个普通技能，别拿普通技能碰瓷抗性，抗性都碰瓷不了抗性",
				"ansory_selftest_moldBreaker": "简单的一句话，里面包含着无数个知识点，可惜现在并不完美",
				"ansory_selftest_rollback": "不算复杂的实现，承载着思索的痕迹，虽然现在也不完美",
				"ansory_selftest_translate": "只是一个单框无选项的实现而已"
			};

			for (const SKILL in SKILLS) {
				lib.translate[`${SKILL}_append`] = `<span style=\"font-family: yuanli\">${SKILLS[SKILL]}</span>`;
			}

			// Backup翻译
			const BACKUPS = {
				"ansory_selftest_translate_multi": "转化"
			};

			for (const BACKUP in BACKUPS) {
				lib.translate[`${BACKUP}_backup`] = BACKUPS[BACKUP];
			}
		},
		precontent(data) {
			if (data.enable) {
				lib.element.content.die = function () {
					"step 0"
					event.forceDie = true;
					if (_status.roundStart == player) {
						_status.roundStart = player.next || player.getNext() || game.players[0];
					}
					if (ui.land?.player == player) {
						game.addVideo("destroyLand");
						ui.land.destroy();
					}
					var unseen = false;
					if (player.classList.contains("unseen")) {
						player.classList.remove("unseen");
						unseen = true;
					}
					var logvid = game.logv(player, "die", source);
					event.logvid = logvid;
					if (unseen) {
						player.classList.add("unseen");
					}
					if (source) {
						game.log(player, "被", source, "杀害");
						if (source.stat[source.stat.length - 1].kill == undefined) {
							source.stat[source.stat.length - 1].kill = 1;
						}
						else {
							source.stat[source.stat.length - 1].kill++;
						}
					}
					else {
						game.log(player, "阵亡")
					}

					game.broadcastAll(function (player) {
						player.classList.add("dead");
						player.removeLink();
						player.classList.remove("turnedover");
						player.classList.remove("out");
						player.node.count.innerHTML = "0";
						player.node.hp.hide();
						player.node.equips.hide();
						player.node.count.hide();
						player.previous.next = player.next;
						player.next.previous = player.previous;
						game.players.remove(player);
						game.dead.push(player);
						_status.dying.remove(player);

						if (lib.config.background_speak) {
							if (lib.character[player.name] && lib.character[player.name][4].contains("die_audio")) {
								game.playAudio("die", player.name);
							}
							else if (lib.character[player.name] && lib.character[player.name][4].some(item => item.match(/^die:/))) {
								let index = lib.character[player.name][4].findIndex(item => {
									if (typeof item == "string") {
										return item.match(/^die:/) != null;
									}
									return false;
								});
								let audio = lib.character[player.name][4][index]
									.replace(/^die:/, "")
									.replace(/ext:/, "extension/");
								game.playAudio("..", audio);
							}
							else if (lib.character[player.name] && lib.character[player.name][4].some(item => item.match(/^died:/))) {
								let index = lib.character[player.name][4].findIndex(item => {
									if (typeof item == "string") {
										return item.match(/^died:/) != null;
									}
									return false;
								});
								let audio = lib.character[player.name][4][index]
									.replace(/^died:/, "")
									.replace(/ext:/, "extension/");
								game.playAudio("..", audio, player.name);
							}
							else {
								game.playAudio("die", player.name, function () {
									game.playAudio("die", player.name.slice(player.name.indexOf("_") + 1));
								});
							}
						}
					}, player);

					game.addVideo("diex", player);
					if (event.animate !== false) {
						player.$die(source);
					}
					if (player.hp != 0) {
						player.changeHp(0 - player.hp, false).forceDie = true;
					}
					"step 1"
					if (player.dieAfter) player.dieAfter(source);
					"step 2"
					event.trigger("die");
					"step 3"
					if (player.isDead()) {
						if (!game.reserveDead) {
							for (var mark in player.marks) {
								player.unmarkSkill(mark);
							}
							while (player.node.marks.childNodes.length > 1) {
								player.node.marks.lastChild.remove();
							}
							game.broadcast(function (player) {
								while (player.node.marks.childNodes.length > 1) {
									player.node.marks.lastChild.remove();
								}
							}, player);
						}
						for (var i in player.tempSkills) {
							player.removeSkill(i);
						}
						var skills = player.getSkills();
						for (var i = 0; i < skills.length; i++) {
							if (lib.skill[skills[i]].temp) {
								player.removeSkill(skills[i]);
							}
						}
						if (_status.characterlist) {
							if (lib.character[player.name]) _status.characterlist.add(player.name);
							if (lib.character[player.name1]) _status.characterlist.add(player.name1);
							if (lib.character[player.name2]) _status.characterlist.add(player.name2);
						}
						event.cards = player.getCards("hejs");
						if (event.cards.length) {
							player.discard(event.cards).forceDie = true;
							//player.$throw(event.cards,1000);
						}
					}
					"step 4"
					if (player.dieAfter2) player.dieAfter2(source);
					"step 5"
					game.broadcastAll(function (player) {
						if (game.online && player == game.me && !_status.over && !game.controlOver && !ui.exit) {
							if (lib.mode[lib.configOL.mode].config.dierestart) {
								ui.create.exit();
							}
						}
					}, player);
					if (!_status.connectMode && player == game.me && !_status.over && !game.controlOver) {
						ui.control.show();
						if (get.config("revive") && lib.mode[lib.config.mode].config.revive && !ui.revive) {
							ui.revive = ui.create.control("revive", ui.click.dierevive);
						}
						if (get.config("continue_game") && !ui.continue_game && lib.mode[lib.config.mode].config.continue_game && !_status.brawl && !game.no_continue_game) {
							ui.continue_game = ui.create.control("再战", game.reloadCurrent);
						}
						if (get.config("dierestart") && lib.mode[lib.config.mode].config.dierestart && !ui.restart) {
							ui.restart = ui.create.control("restart", game.reload);
						}
					}

					if (!_status.connectMode && player == game.me && !game.modeSwapPlayer) {
						// _status.auto=false;
						if (ui.auto) {
							// ui.auto.classList.remove("glow");
							ui.auto.hide();
						}
						if (ui.wuxie) ui.wuxie.hide();
					}

					if (typeof _status.coin == "number" && source && !_status.auto) {
						if (source == game.me || source.isUnderControl()) {
							_status.coin += 10;
						}
					}
					if (source && lib.config.border_style == "auto" && (lib.config.autoborder_count == "kill" || lib.config.autoborder_count == "mix")) {
						switch (source.node.framebg.dataset.auto) {
							case "gold": case "silver": source.node.framebg.dataset.auto = "gold"; break;
							case "bronze": source.node.framebg.dataset.auto = "silver"; break;
							default: source.node.framebg.dataset.auto = lib.config.autoborder_start || "bronze";
						}
						if (lib.config.autoborder_count == "kill") {
							source.node.framebg.dataset.decoration = source.node.framebg.dataset.auto;
						}
						else {
							var dnum = 0;
							for (var j = 0; j < source.stat.length; j++) {
								if (source.stat[j].damage != undefined) dnum += source.stat[j].damage;
							}
							source.node.framebg.dataset.decoration = "";
							switch (source.node.framebg.dataset.auto) {
								case "bronze": if (dnum >= 4) source.node.framebg.dataset.decoration = "bronze"; break;
								case "silver": if (dnum >= 8) source.node.framebg.dataset.decoration = "silver"; break;
								case "gold": if (dnum >= 12) source.node.framebg.dataset.decoration = "gold"; break;
							}
						}
						source.classList.add("topcount");
					}
				};
			}
		},
		config: {
			description: {
				name: `<div class="${META.name}">扩展介绍<font size="5px"}>⇨</font></div>`,
				clear: true,
				onclick() {
					if (this[`${META.name}_description_click`] == undefined) {
						let more = ui.create.div(`.${META.name}_description_click`, `<div style="border: 1px solid gray"><font size=2px>${[
							"自用测试是一个非常规意义上的综合扩展，用于储存Ansory Solution所编写的非常规事务代码（杂项技能）并放入武将当中以便能很好测试代码的实际效果。",
							"本扩展进行滚动更新，不提供常规版本号，以更新日期作为版本信息。",
							"本扩展技能均不涉及抗性，也不修改源代码，故部分技能效果有缺失，请谅解。"
						].join("</br>")
							}</font></div>`);
						this.parentNode.insertBefore(more, this.nextSibling);
						this[`${META.name}_description_click`] = more;
						this.innerHTML = `<div class="${META.name}">扩展介绍<font size="5px">⇩</font></div>`;
					} else {
						this.parentNode.removeChild(this[`${META.name}_description_click`]);
						delete this[`${META.name}_description_click`];
						this.innerHTML = `<div class="${META.name}">扩展介绍<font size="5px"}>⇨</font></div>`;
					};
				}
			},
			version: {
				name: `<span style=\"color:#1688F2\">Version: ${(() => {
					const VERSION = `${META["year"]}.${META["month"]}.${META["date"]}`
					switch (META["branch"]) {
						case "Stable":
							return `${VERSION}`
						case "Testing":
							return `Beta ${VERSION}`
						case "Nightly":
							return `Current ${VERSION}`
						case "Development":
							return `Dev ${VERSION}.${META["times"]}`
						default:
							return `Build ${META.build}`
					}
				})()
					}</span>`,
				clear: true,
				nopointer: true
			},
			info_tab: {
				name: "<p align=\"center\"><span style=\"font-size:18px\">- - - - 扩展信息 - - - -</span></p>",
				clear: true,
				nopointer: true
			},
			authors: {
				name: ["制作者名单：", "- Rintim &lt;rintim@foxmail.com&gt;", "- Angel &lt;3371152010@qq.com&gt;"].join("</br>"),
				clear: true,
				nopointer: true
			},
			infoSupport: {
				name: [
					"设计来源：",
					"- 2HAlO₂·H₂O &lt;3117254928@qq.com&gt;",
					"- 藏海 &lt;2635355476@qq.com&gt;",
				].join("</br>"),
				clear: true,
				nopointer: true
			},
			credits: {
				name: [
					"特别感谢：",
					"- 水乎",
					"- 苏婆玛丽奥",
					"- 诗笺"
				].join("</br>"),
				clear: true,
				nopointer: true
			},
			changelog: {
				name: `<div class="${META.name}">近期更新日志<font size="5px"}>⇨</font></div>`,
				clear: true,
				onclick() {
					if (this[`${META.name}_changelog_click`] == undefined) {
						let more = ui.create.div(`.${META.name}_changelog_click`, `<font size=2px>${[
							"To Be Continued..."
						].join("</br>")
							}</font>`);
						this.parentNode.insertBefore(more, this.nextSibling);
						this[`${META.name}_changelog_click`] = more;
						this.innerHTML = `<div class="${META.name}">近期更新日志<font size="5px">⇩</font></div>`;
					} else {
						this.parentNode.removeChild(this[`${META.name}_changelog_click`]);
						delete this[`${META.name}_changelog_click`];
						this.innerHTML = `<div class="${META.name}">近期更新日志<font size="5px"}>⇨</font></div>`;
					};
				}
			},
			outfrom: {
				name: `<div class="${META.name}">外部来源<font size="5px"}>⇨</font></div>`,
				clear: true,
				onclick() {
					if (this[`${META.name}_outfrom_click`] == undefined) {
						let more = ui.create.div(`.${META.name}_outfrom_click`, `<font size=4px>${[
							"无名杀 Noname",
							""
						].join("</br>")
							}</font>`);
						this.parentNode.insertBefore(more, this.nextSibling);
						this[`${META.name}_outfrom_click`] = more;
						this.innerHTML = `<div class="${META.name}">外部来源<font size="5px">⇩</font></div>`;
					} else {
						this.parentNode.removeChild(this[`${META.name}_outfrom_click`]);
						delete this[`${META.name}_outfrom_click`];
						this.innerHTML = `<div class="${META.name}">外部来源<font size="5px"}>⇨</font></div>`;
					};
				}
			},
			license: {
				name: `<div class="${META.name}">开源协议<font size="5px"}>⇨</font></div>`,
				clear: true,
				onclick() {
					if (this[`${META.name}_license_click`] == undefined) {
						let more = ui.create.div(`.${META.name}_license_click`, `<div style="border: 1px solid gray"><font size=1px>${[
							"<b>BSD 2-Clause License</b>",
							// "",
							"Copyright (c) 2022, Ansory Solution",
							"All rights reserved.",
							// "",
							"Redistribution and use in source and binary forms, with or without",
							"modification, are permitted provided that the following conditions are met:",
							"",
							"1. Redistributions of source code must retain the above copyright notice, this",
							"   list of conditions and the following disclaimer.",
							"",
							"2. Redistributions in binary form must reproduce the above copyright notice,",
							"   this list of conditions and the following disclaimer in the documentation",
							"   and/or other materials provided with the distribution.",
							"",
							"THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\"",
							"AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE",
							"IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE",
							"DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE",
							"FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL",
							"DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR",
							"SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER",
							"CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,",
							"OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE",
							"OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."

						].join("</br>")
							}</font></div>`);
						this.parentNode.insertBefore(more, this.nextSibling);
						this[`${META.name}_license_click`] = more;
						this.innerHTML = `<div class="${META.name}">开源协议<font size="5px">⇩</font></div>`;
					} else {
						this.parentNode.removeChild(this[`${META.name}_license_click`]);
						delete this[`${META.name}_license_click`];
						this.innerHTML = `<div class="${META.name}">开源协议<font size="5px"}>⇨</font></div>`;
					};
				}
			},
			usedLincense: {
				name: `<div class="${META.name}">开源许可证<font size="5px"}>⇨</font></div>`,
				clear: true,
				onclick() {
					if (this[`${META.name}_usedLincense_click`] == undefined) {
						let projects = {
							"Noname": {
								web: "https://github.com/libccy/noname",
								content: [
									"<b>GNU GENERAL PUBLIC LICENSE</b>",
									"Version 3, 29 June 2007",
									"Copyright (C) 2007 Free Software Foundation, Inc. &lt;http://fsf.org/&gt;",
									"Everyone is permitted to copy and distribute verbatim copies",
									"of this license document, but changing it is not allowed.",
									"<b>Preamble</b>",
									"The GNU General Public License is a free, copyleft license for",
									"software and other kinds of works.",
									"The licenses for most software and other practical works are designed",
									"to take away your freedom to share and change the works.  By contrast,",
									"the GNU General Public License is intended to guarantee your freedom to",
									"share and change all versions of a program--to make sure it remains free",
									"software for all its users.  We, the Free Software Foundation, use the",
									"GNU General Public License for most of our software; it applies also to",
									"any other work released this way by its authors.  You can apply it to",
									"your programs, too.",
									"When we speak of free software, we are referring to freedom, not",
									"price.  Our General Public Licenses are designed to make sure that you",
									"have the freedom to distribute copies of free software (and charge for",
									"them if you wish), that you receive source code or can get it if you",
									"want it, that you can change the software or use pieces of it in new",
									"free programs, and that you know you can do these things.",
									"To protect your rights, we need to prevent others from denying you",
									"these rights or asking you to surrender the rights.  Therefore, you have",
									"certain responsibilities if you distribute copies of the software, or if",
									"you modify it: responsibilities to respect the freedom of others."
								].join("</br>")
							}
						};
						let strings = new String;
						for (let item in projects) {
							strings += `<font size=4px>${item}</font></br><font size=2px>${projects[item].web}</font></br>`;
							strings += `<div style="border: 1px solid gray"><font size=1px>${projects[item].content}</font></div></br>`;
						}
						strings = strings.substring(0, strings.lastIndexOf("</br>"));
						let more = ui.create.div(`.${META.name}_usedLincense_click`, strings);
						this.parentNode.insertBefore(more, this.nextSibling);
						this[`${META.name}_usedLincense_click`] = more;
						this.innerHTML = `<div class="${META.name}">开源许可证<font size="5px">⇩</font></div>`;
					} else {
						this.parentNode.removeChild(this[`${META.name}_usedLincense_click`]);
						delete this[`${META.name}_usedLincense_click`];
						this.innerHTML = `<div class="${META.name}">开源许可证<font size="5px"}>⇨</font></div>`;
					};
				}
			},
			copyrights: {
				name: ["<span style=\"color:#1688F2\"><p align=\"center\">Copyright (c) 2022", "Ansory Solution", "All Rights Reserved</p></span>"].join("</br>"),
				clear: true,
				nopointer: true
			}
		},
		help: {},
		package: {
			character: {
				character: {
					"ansory_lrEtc-OTH001": ["none", "other", 4, ["ansory_selftest_doubleUse", "ansory_selftest_useAgain"], ["forbidai", "ext:自用测试/res/image/character/rEtc-OTH001.jpg", "die:ext:自用测试/res/audio/die/rEtc-OTH001.ogg", "des:连点器，又称小复读机，可以将牌反复使用：有这样一个东西，杀别人能获得牌，决斗别人也能获得牌，那么这个东西一定是孙策。"]],
					"ansory_brEtc-OTH002": ["none", "other", 4, ["ansory_selftest_crit", "ansory_selftest_critTime", "ansory_selftest_critViad"], ["forbidai", "ext:自用测试/res/image/character/rEtc-OTH002.jpg"]],
					"ansory_jrEtc-OTH003": ["none", "other", 4, ["ansory_selftest_inherit", "ansory_selftest_addSum"], ["forbidai"]],
					"ansory_prEtc-OTH004": ["none", "other", 4, ["ansory_selftest_filter", "ansory_selftest_shield"], ["forbidai"]],
					"ansory_zrEtc-OTH005": ["none", "other", 4, ["ansory_selftest_moldBreaker", "ansory_selftest_rollback"], ["forbidai"]],
					"ansory_crEtc-OTH006": ["none", "other", 4, ["ansory_selftest_store", "ansory_selftest_load"], ["forbidai"]],
					"ansory_lrEtc-OTH007": ["none", "other", 4, ["ansory_selftest_faultTolerance", "ansory_selftest_robustness"], ["forbidai"]],
					"ansory_srEtc-OTH008": ["none", "other", 4, ["ansory_selftest_shootarchery"], ["forbidai"]],
					"ansory_mrEtc-OTH009": ["none", "other", 4, [], ["forbidai"]],
					"ansory_srEtc-OTH00A": ["none", "other", 4, ["ansory_selftest_translate"], ["forbidai"]]
				},
				translate: {
					"ansory_lrEtc-OTH001": "连点器",
					"ansory_brEtc-OTH002": "暴击器",
					"ansory_jrEtc-OTH003": "继承器",
					"ansory_prEtc-OTH004": "屏蔽器",
					"ansory_zrEtc-OTH005": "综合器",
					"ansory_crEtc-OTH006": "存储器",
					"ansory_lrEtc-OTH007": "鲁棒器",
					"ansory_srEtc-OTH008": "射箭器",
					"ansory_mrEtc-OTH009": "膜法器",
					"ansory_srEtc-OTH00A": "使用器"
				},
			},
			card: {
				card: {
				},
				translate: {
				},
				list: [],
			},
			skill: {
				skill: {
					// rEtc-OTH001 连点器
					"ansory_selftest_doubleUse": {
						audio: "ext:自用测试/res/audio/skill:1",
						trigger: {
							player: "useCardEnd"
						},
						filter(event, player, _name) {
							if (event.doubleUse || !["basic", "trick"].includes(get.type(event.card))) return false
							event.xtargets = event.targets.filter(current => current.isAlive() && player.canUse(event.card, current, false));
							return event.xtargets.length > 0
						},
						content(trigger) {
							// 重置useCard事件
							// event.finished 决定一个事件是否结束
							// event._triggered则决定事件的运行
							// 
							trigger.finished = false;
							trigger._triggered = 2;
							//
							trigger.num = 0;
							trigger.step = 0;
							trigger.targets = trigger.xtargets;
							delete trigger.xtargets;
							//
							trigger.addCount = false;
							//
							trigger.doubleUse = true;
						}
					},
					"ansory_selftest_useAgain": {
						audio: "ext:自用测试/res/audio/skill:1",
						locked: true,
						init(player, skill) {
							player.storage[skill] = new Array;
						},
						group: ["ansory_selftest_useAgain_refresh", "ansory_selftest_useAgain_gain"]
					},
					"ansory_selftest_useAgain_refresh": {
						audio: false,
						trigger: {
							global: ["phaseBefore", "phaseAfter"]
						},
						charlotte: true,
						sourceSkill: "ansory_selftest_useAgain",
						silent: true,
						content(player) {
							player.storage["ansory_selftest_useAgain"].length = 0;
							player.removeGaintag("ansory_selftest_useAgain");
						}
					},
					"ansory_selftest_useAgain_gain": {
						audio: "ansory_selftest_useAgain",
						trigger: {
							player: "useCardAfter"
						},
						locked: true,
						sourceSkill: "ansory_selftest_useAgain",
						forced: true,
						filter(event, player, _name) {
							if (event.doubleUse || !["basic", "trick"].includes(get.type(event.card))) return false
							event.gainCards = event.cards.filter(card => !get.owner(card) && get.position(card) == "d" && !player.storage["ansory_selftest_useAgain"].includes(card));
							return event.gainCards.length > 0
						},
						content(trigger, player) {
							player.gain(trigger.gainCards, "gain2").gaintag.push("ansory_selftest_useAgain");
							player.storage["ansory_selftest_useAgain"].addArray(trigger.gainCards);
						}
					},
					// rEtc-OTH002 暴击器
					"ansory_selftest_crit": {
						locked: true,
						global: "ansory_selftest_crit_damage",
						mark: true,
						markText: "暴",
						intro: {
							name: "暴击率",
							content: (_storage, player, _skill) => `暴击几率：${game.checkMod(_status.event, player, 0.05, "critChange", player)}`
						}
					},
					"ansory_selftest_crit_damage": {
						trigger: {
							source: "damageBegin1"
						},
						charlotte: true,
						silent: true,
						sourceSkill: "ansory_selftest_crit",
						filter(event, player, _name) {
							if (!event.notLink()) return false
							const rand = get.isLuckyStar(player) ? 0 : event.getRand("crit");
							return rand < game.checkMod(event, player, 0.05, "critChange", player)
						},
						content(trigger, player) {
							game.log(player, "会心一击！");
							if (player.hasSkillTag("critDamageModify")) trigger.num = game.checkMod(trigger, player, trigger.num, "critDamage", player);
							else ++trigger.num;
							trigger.crit = true;
						}
					},
					"ansory_selftest_critTime": {
						locked: true,
						mod: {
							critDamage: (_event, _player, num) => num * 2
						},
						ai: {
							critDamageModify: true
						}
					},
					"ansory_selftest_critViad": {
						trigger: {
							global: "damageEnd"
						},
						locked: true,
						forced: true,
						init(player, skill) {
							player.storage[skill] = 0;
						},
						filter: (event, player, _name) => !event.crit && event.num > 0 && game.checkMod(event, player, 0.05, "critChange", player) < 1,
						content(event, trigger, player) {
							player.storage[event.name] += 0.05 * trigger.num;
							while (game.checkMod(event, player, 0.05, "critChange", player) > 1) player.storage[event.name] -= 0.01;
						},
						mod: {
							critChange: (_event, player, num) => num + player.storage["ansory_selftest_critViad"]
						}
					},
					// rEtc-OTH003 继承器
					"_ansory_selftest_inherit": {
						audio: false,
						trigger: {
							global: "phaseBefore",
							player: "enterGame",
						},
						charlotte: true,
						silent: true,
						filter: (event, player, _name) => player == game.me && lib.config["ansory_selftest_inherit_to_do_the_inherit"] && (event.name != "phase" || game.phaseNumber == 0),
						content(player) {
							"step 0"
							player.logSkill("ansory_selftest_inherit");
							"step 1"
							{
								player.gainMaxHp();
								if (lib.config["ansory_selftest_inherit_next_draw_cards_length"]) player.draw(lib.config["ansory_selftest_inherit_next_draw_cards_length"]);
								lib.config["ansory_selftest_inherit_to_do_the_inherit"] = 0;
								lib.config["ansory_selftest_inherit_next_draw_cards_length"] = 0;
								game.saveConfigValue("ansory_selftest_inherit_to_do_the_inherit");
								game.saveConfigValue("ansory_selftest_inherit_next_draw_cards_length");
							}
						}
					},
					"ansory_selftest_inherit": {
						audio: false,
						trigger: {
							player: "dieBegin"
						},
						locked: true,
						forced: true,
						forceDie: true,
						firstDo: true,
						skillAnimation: true,
						animationColor: "gray",
						content(player) {
							// 原理：通过无名杀自带的设置存储来记录手牌数，并于下局游戏开始后读取并清零
							// 防止意外发生，名字取得非常长，应该不会有重复
							lib.config["ansory_selftest_inherit_to_do_the_inherit"] = 1;
							lib.config["ansory_selftest_inherit_next_draw_cards_length"] = player.countCards("h");
							game.saveConfigValue("ansory_selftest_inherit_to_do_the_inherit");
							game.saveConfigValue("ansory_selftest_inherit_next_draw_cards_length");
						}
					},
					"_ansory_selftest_addSum": {
						audio: false,
						trigger: {
							player: "phaseBefore"
						},
						charlotte: true,
						silent: true,
						lastDo: true,
						content(player) {
							lib.config["ansory_selftest_addSum_countCards_player_card"] = player.countCards("h");
							game.saveConfigValue("ansory_selftest_addSum_countCards_player_card");
						}
					},
					"ansory_selftest_addSum": {
						audio: false,
						trigger: {
							global: "phaseBefore",
							player: "enterGame",
						},
						locked: true,
						forced: true,
						filter: (event, player, _name) => player == game.me && lib.config["ansory_selftest_addSum_countCards_player_card"] > 0 && (event.name != "phase" || game.phaseNumber == 0),
						content(player) {
							player.draw(lib.config["ansory_selftest_addSum_countCards_player_card"]);
						}
					},
					// rEtc-OTH004 屏蔽器
					"ansory_selftest_filter": {
						// 没想好过滤该怎么设计，先水了再说
						mod: {
							targetEnabled: (card, _player, _target, result) => get.tag(card, "damage") ? false : result
						}
					},
					"ansory_selftest_shield": {
						// 主技能，本来是个抗性，秉持着不写抗性的原则，这就是个普通技能
						// 抗性技能是R999，请出示身份证再跟我说（🤔）
						audio: false,
						trigger: {
							global: "gameStart"
						},
						locked: true,
						forced: true,
						content(player) {
							game.players.filter(current => current != player).forEach(current => {
								player.line(current);
								const skills = current.skills.slice(0);
								// 原理：判断有没有translate，没有基本上可以判定无名字/描述
								// 其他神奇的情况不想涉及
								skills.filter(skill => get.translation(skill) != skill || get.translation(`${skill}_info`) != `${skill}_info`).forEach(skill => {
									current.removeSkill(skill, true);
								});
							});
						}
					},
					// rEtc-OTH005 综合器
					// 综合器的来源有点复杂，而且有些魔幻。
					// 简单来说就是一个人写了两个我曾写过的技能，然后发在了贴吧里。其中一个技能是他独自探索出来的，另一个技能我教了他点皮毛。
					// 虽然这事本来不应该被提及，我在这说也有点莫名其妙
					// 但正巧我和他有了点矛盾，虽然我对此的感觉有点微妙与复杂，但我没啥想说的，只是把这两个技能放到这里留作纪念而已。
					// 顺带一提，两个技能均没有写AI，请友善对待人工智障
					// From Rintim
					"ansory_selftest_moldBreaker": {
						// 破格原理
						// checkMod通过game.expandSkills来展开技能，而这个函数的方式是将group技能放置在所有普通技能之后
						// 故当此技能group发挥功能的技能后，这个技能将会在其他技能check完后调用
						// 从而覆盖各种debuff对卡牌使用的影响
						// 但如果存在group的debuff，这个技能就会失效
						// 本扩展不对源代码进行修改，故不打算解决此问题
						group: "ansory_selftest_moldBreaker_mod"
					},
					"ansory_selftest_moldBreaker_mod": {
						// 关于卡牌使用、打出与弃置情况，这里会依照checkMod的原理遍历自身自带技能的Mod
						// 对于时间复杂度来说，o(2n-c)效率等效于o(n)，故无明显的性能损失
						// 均以player.storage._checkAgain作为flag来防止无限递归
						sourceSkill: "ansory_selftest_moldBreaker",
						checkMod(name, card, player, result) {
							const SKILLS = game.expandSkills(player.getStockSkills(true, true).concat(player.getSkills("e")).concat(lib.skill.global));
							return SKILLS.reduce((result, skill) => {
								const INFO = get.info(skill);
								if (!INFO || !INFO.mod || !INFO.mod[name]) return result
								let new_result = INFO.mod[name](card, player, result);
								return (typeof result != "object" && new_result != undefined) ? new_result : result
							}, result);
						},
						mod: {
							cardUsable(card, player, num) {
								if (player.storage._checkAgain) return num
								player.storage._checkAgain = true;
								// 从此处开始是getCardUsable的内容，用于获取卡牌原本的使用次数
								let acard = get.autoViewAs(card, null, player);
								let anum = get.info(acard).usable;
								if (typeof anum == "function") num = anum(card, player);
								// 此处是checkMod的内容
								num = lib.skill["ansory_selftest_moldBreaker_mod"].checkMod("cardUsable", card, player, num);
								// 两者调用完毕，开始收尾
								delete player.storage._checkAgain;
								return typeof num == "number" ? num : Infinity
							},
							cardEnable(card, player, result) {
								if (player.storage._checkAgain) return result
								player.storage._checkAgain = true;
								// 此处是checkMod的内容
								result = lib.skill["ansory_selftest_moldBreaker_mod"].checkMod("cardEnable", card, player, "unchanged");
								// 调用完毕，开始收尾
								delete player.storage._checkAgain;
								return result
							},
							cardEnabled2(card, player, result) {
								if (player.storage._checkAgain) return result
								player.storage._checkAgain = true;
								// 此处是checkMod的内容
								result = lib.skill["ansory_selftest_moldBreaker_mod"].checkMod("cardEnable2", card, player, "unchanged");
								// 调用完毕，开始收尾
								delete player.storage._checkAgain;
								return result
							},
							cardRespondable(card, player, result) {
								if (player.storage._checkAgain) return result
								player.storage._checkAgain = true;
								// 此处是checkMod的内容
								result = lib.skill["ansory_selftest_moldBreaker_mod"].checkMod("cardRespondable", card, player, "unchanged");
								// 调用完毕，开始收尾
								delete player.storage._checkAgain;
								return result
							},
							cardDiscardable(card, player, result) {
								if (player.storage._checkAgain) return result
								player.storage._checkAgain = true;
								// 此处是checkMod的内容
								result = lib.skill["ansory_selftest_moldBreaker_mod"].checkMod("cardDiscardable", card, player, "unchanged");
								// 调用完毕，开始收尾
								delete player.storage._checkAgain;
								return result
							}
						}
					},
					"ansory_selftest_rollback": {
						// 回溯的原理是将当前事件取消，并善后
						// 因为使用牌与打出牌均存在特殊情况，故只能特殊处理，无法泛化
						// 有些特殊情况可能没考虑上，如果存在请反馈给我
						// （毕竟当初写着玩，也没发出去过，我只能测试我知道的）（From Rintim with small sound）
						audio: false,
						trigger: {
							// useCard1在useCard之前，适合作为时机
							// 两者的Before与Begin都位于将牌移除手牌区前，不合适
							global: ["useCard1", "respond"]
						},
						filter: (event, player, _name) => event.player != player,
						logTarget: "player",
						content(trigger, player) {
							"step 0"
							// 取消当前事件
							trigger.cancel();
							// 本技能只特殊处理无名杀本体拥有的特殊事件，如果有遗漏记得提醒我
							// 通过分析可知，能将事件分为三类：存在响应牌，使用牌与打出牌
							if (trigger.respondTo) {
								// 在响应牌的事件中，无懈是唯一特殊的，故以此判断
								if (trigger.card.name == "wuxie") {
									trigger.parent.goto(2);
									// 此处调用了无懈的filterPlayer，用于判断当前角色能否继续使用无懈
									let filter = function (current, event, trigger, player) {
										if (event.nowuxie) return false;
										if (event.directHit?.contains(current)) return false;
										if (event.triggername == "phaseJudge") {
											if (game.checkMod(trigger.card, player, current, "unchanged", "wuxieJudgeEnabled", current) == false) return false
											if (game.checkMod(trigger.card, player, current, "unchanged", "wuxieJudgeRespondable", player) == false) return false
											if (event.stateplayer && event.statecard && (game.checkMod(event.statecard, event.stateplayer, player, current, "unchanged", "wuxieRespondable", event.stateplayer) == false)) return false
										}
										else {
											if (!event.statecard && trigger.getParent().directHit.contains(current)) return false
											if (game.checkMod(trigger.card, player, trigger.target, current, "unchanged", "wuxieEnabled", current) == false) return false
											if (game.checkMod(trigger.card, player, trigger.target, current, "unchanged", "wuxieRespondable", player) == false) return false
											if (event.stateplayer && event.statecard && (game.checkMod(event.statecard, event.stateplayer, trigger.player, current, "unchanged", "wuxieRespondable", event.stateplayer) == false)) return false
										}
										return current.hasWuxie()
									};
									if (filter(trigger.player, trigger.parent, trigger.parent._trigger, trigger.player)) trigger.parent.list.unshift(trigger.player);
								}
								// 非无懈的情况
								// 无限遍历寻找与打出牌名相同的事件，并令事件回退步骤
								// 但响应牌应该也存在特殊情况，不过我不清楚，如果遇上的话请及时反馈
								else {
									let index = 0;
									while (true) {
										let evt = trigger.getParent(index++);
										if (trigger.respondTo[1].name == evt.name) {
											evt.goto(evt.step);
											break;
										}
									}
								}
							}
							// 使用牌的情况
							else if (trigger.name == "useCard") {
								// 这里再取消计数本质上没有必要
								// 但他加了，保守起见我也加了
								trigger.addCount = false;
								// 将本次的次数去掉
								let stat = trigger.player.getStat("card");
								if (stat?.[trigger.card.name]) --stat[trigger.card.name];
							}
							// 只打出牌的情况，需要考虑改判技能
							// 由于我不了解所有的改判技能，如果有特殊情况，请反馈给我
							else if (get.tag(trigger.parent.name, "rejudge")) trigger.parent.goto(0)
							// 收尾工作，去除本来事件的历史记录
							// 关于尾声，他本来打算管技能的次数，最后不知为何去了
							// 我的技能只是令使用/打出事件本身无视发生，不涉及技能，故就此终结
							// 话虽如此，给个简单的思路，技能都会伴随trigger事件作为父事件，检测trigger后获取下一级即使技能事件
							// 而技能事件的名字就是技能名，就能对技能本身进行修改了
							trigger.player.getHistory(trigger.name == "useCard" ? "useCard" : "respond").pop();
							game.log(player, "取消了", trigger.player, "的", trigger.name == "useCard" ? "使用" : "打出", "事件");
							game.delay();
						}
					},
					// rEtc-OTH006 存储器
					// 原理同继承器
					"ansory_selftest_store": {
						audio: false,
						usable: 1,
						enable: "phaseUse",
						filterCard: (card, player, _target) => lib.filter.cardDiscardable(card, player),
						selectCard: [1, Infinity],
						position: "he",
						content() {
							if (!lib.config["ansory_selftest_store_to_store_the_card_info_hiany"]) lib.config["ansory_selftest_store_to_store_the_card_info_hiany"] = new Array;
							lib.config["ansory_selftest_store_to_store_the_card_info_hiany"].addArray(cards.map(card => ({
								name: card.name,
								suit: card.suit,
								number: card.number,
								nature: card.nature
							})));
							game.saveConfigValue("ansory_selftest_store_to_store_the_card_info_hiany");
							player.markSkill("ansory_selftest_store");
							// game.addVideo("storage", player, ["ansory_selftest_store", JSON.parse(JSON.stringify(lib.config["ansory_selftest_store_to_store_the_card_info_hiany"]))]);
						},
						mark: true,
						intro: {
							mark(dialog, _storage, _player) {
								if (!lib.config["ansory_selftest_store_to_store_the_card_info_hiany"].length) return dialog.addText("暂无牌存储于游戏内部")
								dialog.addText(lib.config["ansory_selftest_store_to_store_the_card_info_hiany"].map(cardinfo => `${lib.translate[cardinfo.nature] || ""}${get.translation(cardinfo.name)}(${get.translation(cardinfo.suit)}${cardinfo.number})`).join(", "));
							},
							markcount: (..._args) => lib.config["ansory_selftest_store_to_store_the_card_info_hiany"].length
						}
					},
					"ansory_selftest_load": {
						audio: false,
						usable: 1,
						enable: "phaseUse",
						init(_player, skill) {
							lib.translate[`${skill}_backup`] = get.translation(skill);
						},
						filter: (..._args) => lib.config["ansory_selftest_store_to_store_the_card_info_hiany"].length > 0,
						chooseButton: {
							dialog(..._args) {
								let result = new Array;
								for (let i = 0; i < lib.config["ansory_selftest_store_to_store_the_card_info_hiany"].length; ++i) {
									const cardinfo = lib.config["ansory_selftest_store_to_store_the_card_info_hiany"][i];
									let card = get.cardPile(card => Object.keys(cardinfo).every(name => cardinfo[name] == card[name]));
									result.push((card && !result.includes(card)) ? card : game.createCard(cardinfo))
								}
								return ui.create.dialog("读取", result);
							},
							select: [1, Infinity],
							backup: (links, player) => ({
								audio: "ansory_selftest_load",
								cards: links,
								filterCard: (..._args) => false,
								selectCard: -1,
								content: () => {
									const cardxs = lib.skill["ansory_selftest_load_backup"].cards;
									player.gain(cardxs, "gain2");
									// /-/
									for (let i = 0; i < lib.config["ansory_selftest_store_to_store_the_card_info_hiany"].length; ++i) {
										const item = lib.config["ansory_selftest_store_to_store_the_card_info_hiany"][i];
										if (cardxs.some(card => Object.keys(item).every(name => item[name] == card[name])))
											lib.config["ansory_selftest_store_to_store_the_card_info_hiany"].splice(i--, 1);
									}
									game.updateRoundNumber();
									game.saveConfigValue("ansory_selftest_store_to_store_the_card_info_hiany");
									player[lib.config["ansory_selftest_store_to_store_the_card_info_hiany"].length ? "markSkill" : "unmarkSkill"]("ansory_selftest_store");
								}
							})
						}
					},
					// rEtc-OTH007 鲁棒器
					"ansory_selftest_faultTolerance": {
						audio: false,
						trigger: {
							global: ["drawBegin", "damageBegin3", "chooseToDiscardBegin"]
						},
						locked: true,
						forced: true,
						filter: (event, _player, _name) => event.name == "chooseToDiscard" ? event.selectCard[0] > 1 : event.num > (event.name == "draw" ? 2 : 1), /*(event.name != "chooseToDiscard" || event.getParent("phaseDiscard", true)) && */
						content(trigger) {
							if (trigger.name == "chooseToDiscard") trigger.selectCard[0] = 1;
							else trigger.num = (trigger.name == "draw" ? 2 : 1);
						},
						global: "ansory_selftest_faultTolerance_mod"
					},
					"ansory_selftest_faultTolerance_mod": {
						mod: {
							cardUsable: (_card, _player, num) => (num || 1) + game.players.filter(current => !current.isOut() && current.hasSkill("ansory_selftest_faultTolerance")).reduce((result, current) => result + current.maxHp, 0)
						}
					},
					"ansory_selftest_robustness": {
						audio: false,
						trigger: {
							player: ["recoverEnd", "damageEnd", "loseHpEnd"]
						},
						locked: true,
						forced: true,
						filter: (event, _player, _name) => event.num > 0,
						content(trigger, player) {
							player[trigger.name == "recover" ? "loseMaxHp" : "gainMaxHp"](trigger.num);
							if (trigger.name == "recover") {
								let playerCards = player.getCards("he");
								if (playerCards.length > (trigger.num * player.getDamagedHp())) player.chooseToDiscard(trigger.num * player.getDamagedHp(), true);
								else player.discard(playerCards);
							}
							else player.draw(trigger.num * player.getDamagedHp());
						},
						global: "ansory_selftest_robustness_mod"
					},
					"ansory_selftest_robustness_mod": {
						mod: {
							cardUsable: (_card, player, num) => num && (num - game.players.filter(current => !current.isOut() && current != player && current.hasSkill("ansory_selftest_robustness")).reduce((result, current) => result + current.getDamagedHp() + 1, 0))
						}
					},
					// rEtc-OTH008 射箭器
					"ansory_selftest_shootarchery": {
						audio: false,
						locked: true,
						group: ["ansory_selftest_shootarchery_mod", "ansory_selftest_shootarchery_modify", "ansory_selftest_shootarchery_respond"]
					},
					"ansory_selftest_shootarchery_mod": {
						audio: "ansory_selftest_shootarchery",
						locked: true,
						sourceSkill: "ansory_selftest_shootarchery",
						mod: {
							targetInRange: (card, _player, _target, result) => get.name(card) == "sha" ? true : result,
							cardUsable: (card, _player, num) => get.name(card) == "sha" ? Infinity : num
						}
					},
					"ansory_selftest_shootarchery_modify": {
						audio: "ansory_selftest_shootarchery",
						trigger: {
							player: "useCard1"
						},
						locked: true,
						forced: true,
						sourceSkill: "ansory_selftest_shootarchery",
						filter: (event, _player, _name) => get.name(event.card) == "sha",
						content(trigger, player) {
							game.log(player, "将", trigger.card, "改为了", `<span class=\"yellowtext\">万箭齐发【${get.translation(trigger.card.suit)}${[1, 11, 12, 13].includes(trigger.card.number) ? { "1": "A", "11": "J", "12": "Q", "13": "K" }[trigger.card.number] : trigger.card.number
								}】</span>`);
							trigger.card.name = "wanjian";
							player.tryCardAnimate(trigger.card, "wanjian", "metal");
						}
					},
					"ansory_selftest_shootarchery_respond": {
						audio: "ansory_selftest_shootarchery",
						trigger: {
							global: ["useCard", "respond", "dyingAfter"]
						},
						locked: true,
						forced: true,
						sourceSkill: "ansory_selftest_shootarchery",
						logTarget: "player",
						filter: (event, player, name) => event.player.isAlive() && event.getParent(3 + (name == "useCard") * 2).player == player && event.getParent(2 + (name == "useCard") * 2).name == "wanjian",
						content(event, trigger, player) {
							let evt = trigger.parent.parent;
							if (event.triggername == "useCard") evt = evt.parent.parent;
							"step 0"
							trigger.player.draw();
							if (trigger.player == player) {
								event.goto(2);
								event.selfDo = true
								player.chooseBool().set("prompt", `是否发动【正击】使万箭齐发对${get.translation(evt.target.name)}重新结算？`)
							}
							"step 1"
							player.gainPlayerCard(`正击：获得${get.translation(trigger.player.name)}区域内一张牌`, trigger.player, true, 1, "hej")
								.set("visibleMove", true);
							// trigger.player.chooseCard(`正击：将一张牌交给${get.translation(player.name)}，否则重新执行${get.translation(trigger.getParent(2).card)}的效果`, 1, "he", card => get.type(card) != "basic").set("ai", card => 10 - get.value(card));
							"step 2"
							if (result.bool && (event.selfDo || get.type(result.cards[0]) != "basic")) {
								player.line(evt.target, "metal");
								game.log(player, "令", evt.card, "重新对", evt.target, "结算");
								if (evt.step > 0) evt.goto(evt.step);
								else evt.parent.targets.splice(evt.num, 0, evt.target);
								game.delay();
							}
						}
					},
					// rEtc-OTH009 膜法器
					"ansory_selftest_onlyself": {
						audio: false,
						trigger: {
							source: "damageBegin1",
							player: "damageBegin3",
						},
						locked: true,
						zhuanhuanji: true,
						forced: true,
						mark: true,
						marktext: "☯",
						intro: {
							content: function (storage, player, skill) {
								if (player.storage.nzry_juzhan == true) return "当你使用【杀】指定一名角色为目标后，你可以获得其一张牌，然后你本回合内不能再对其使用牌";
								return "当你成为其他角色【杀】的目标后，你可以与其各摸一张牌，然后其本回合内不能再对你使用牌";
							},
						},
					},
					// rEtc-OTH00A 使用器
					"ansory_selftest_translate": {
						audio: false,
						enable: ["chooseToUse", "chooseToRespond"],
						filter(event, player) {
							if (player.countCards("hes")) {
								lib.skill["ansory_selftest_translate"].attribute.cards = lib.inpile.reduce((result, name) => {
									if (event.filterCard({ name }, player, event)) {
										if (name == "sha") return result.concat(lib.inpile_nature.map(nature => ["基本", "", "sha", nature]));
										else if (get.type(name) == "basic") return result.concat([["基本", "", name]])
										else if (get.type(name) == "trick") return result.concat([["锦囊", "", name]])
									}
									return result
								}, new Array);
							}
						},
						// hiddenCard似乎优先级调用更高，估计之后有啥条件的话得二次遍历了，悲
						hiddenCard: (player, _card) => player.countCards("hes") > 0,
						content() {
							event.finish()
						},
						group: ["ansory_selftest_translate_single", "ansory_selftest_translate_multi"],
						attribute: {
							cards: new Array
						}
					},
					"ansory_selftest_translate_single": {
						audio: /* "ansory_selftest_translate" */ false,
						enable: ["chooseToUse", "chooseToRespond"],
						sourceSkill: "ansory_selftest_translate",
						filter: (..._args) => lib.skill["ansory_selftest_translate"].attribute.cards.length == 1,
						viewAs: (..._args) => ({ name: lib.skill["ansory_selftest_translate"].attribute.cards[0][2] }),
						viewAsFilter: (..._args) => lib.skill["ansory_selftest_translate"].attribute.cards.length == 1,
						popname: true,
						filterCard: lib.filter.all,
						selectCard: [1, Infinity],
						hiddenCard: (_player, name) => lib.skill["ansory_selftest_translate"].attribute.cards[0][2] == name,
						position: "hes",
						prompt: (..._args) => `将任意张牌当作${get.translation(lib.skill["ansory_selftest_translate"].attribute.cards[0][2])}使用或打出`,
						ai: {
							respondShan: true,
							respondSha: true,
							skillTagFilter: (..._args) => lib.skill["ansory_selftest_translate"].attribute.cards.length == 1
						}
					},
					"ansory_selftest_translate_multi": {
						audio: "ansory_selftest_translate",
						enable: ["chooseToUse", "chooseToRespond"],
						sourceSkill: "ansory_selftest_translate",
						filter: (..._args) => lib.skill["ansory_selftest_translate"].attribute.cards.length > 1,
						chooseButton: {
							dialog: (_event, _player) => ui.create.dialog("转化", [lib.skill["ansory_selftest_translate"].attribute.cards, "vcard"]),
							filter: (button, player) => _status.event.getParent().filterCard({ name: button.link[2] }, player, _status.event.getParent()),
							backup: (links, player) => ({
								audio: /* "ansory_selftest_translate" */ false,
								filterCard: lib.filter.all,
								selectCard: [1, Infinity],
								popname: true,
								check: (card) => 6 - get.value(card),
								position: "hes",
								viewAs: { 
									name: links[0][2], 
									nature: links[0][3] 
								},
							}),
							prompt: (links, _player) => `将任意张牌当做${(get.translation(links[0][3]) || "")}${get.translation(links[0][2])}使用或打出`
						},
						hiddenCard: (_player, name) => lib.skill["ansory_selftest_translate"].attribute.cards.some(item => item[2] == name),
						ai: {
							respondShan: true,
							respondSha: true,
							skillTagFilter(_player, tag, _arg) {
								if (lib.skill["ansory_selftest_translate"].attribute.cards.length <= 1) return false
								const name = tag == "respondSha" ? "sha" : "shan";
								return lib.skill["ansory_selftest_translate"].attribute.cards.some(item => item[2] == name)
							},
						}
					}
				},
				translate: {
					// rEtc-OTH001 连点器
					"ansory_selftest_doubleUse": "连发",
					"ansory_selftest_doubleUse_info": "每次使用牌限一次，当你使用基本牌或普通锦囊牌结算时，你可重复结算此牌效果。",
					"ansory_selftest_useAgain": "复用",
					"ansory_selftest_useAgain_gain": "复用",
					"ansory_selftest_useAgain_info": "锁定技，每张牌每回合限一次，当你使用基本牌或普通锦囊牌结算后，若此牌未触发“连发”效果，你获得此牌。",
					// rEtc-OTH002 暴击器
					"ansory_selftest_crit": "暴击",
					"ansory_selftest_crit_damage": "暴击",
					"ansory_selftest_crit_info": "锁定技，当你在场时，所有角色有5%的机率触发暴击效果（非铁索造成伤害+1）",
					"ansory_selftest_critTime": "聚击",
					"ansory_selftest_critTime_info": "锁定技，你的暴击效果改为造成伤害*2。",
					"ansory_selftest_critViad": "怒积",
					"ansory_selftest_critViad_info": "锁定技，当一名角色造成一点伤害后，若该伤害未触发暴击效果，本局游戏你触发暴击的概率+5%（不超过100%）",
					// rEtc-OTH003 继承器
					"ansory_selftest_inherit": "继承",
					"ansory_selftest_inherit_info": "锁定技，当你死亡后，你于下一局游戏开始后增加一点体力上限，然后摸X张牌（X为你死亡时的手牌数）",
					"ansory_selftest_addSum": "累加",
					"ansory_selftest_addSum_info": "锁定技，游戏开始时，你摸X张牌（X为自本扩展加载后你最后一次进回合时的手牌数）",
					// rEtc-OTH004 屏蔽器
					"ansory_selftest_filter": "过滤",
					"ansory_selftest_filter_info": "锁定技，你无法成为其他角色伤害牌的目标。",
					"ansory_selftest_shield": "屏蔽",
					"ansory_selftest_shield_info": "锁定技，游戏开始时，其他角色失去含技能名称或技能描述的技能。",
					// rEtc-OTH005 综合器
					"ansory_selftest_moldBreaker": "破格",
					"ansory_selftest_moldBreaker_info": "锁定技，你使用、打出与弃置牌不受非武将牌上的技能影响。",
					"ansory_selftest_rollback": "回溯",
					"ansory_selftest_rollback_info": "当一名其他角色使用或打出牌时，你可令此牌失效，然后令此次使用/打出事件视为无事发生。",
					// rEtc-OTH006 存储器
					"ansory_selftest_store": "储存",
					"ansory_selftest_store_info": "出牌阶段限一次，你可以弃置任意张牌，然后将这些牌的数据存储在游戏内部。",
					"ansory_selftest_load": "读取",
					"ansory_selftest_load_info": "出牌阶段限一次，你可以获取储存在游戏内部的任意张牌。",
					// rEtc-OTH007 鲁棒器
					"ansory_selftest_faultTolerance": "容错",
					"ansory_selftest_faultTolerance_info": "锁定技，场上所有角色使用牌的次数+X（若原本无次数限制则将次数改为X+1）；当一名角色即将摸三张以上的牌/受到两点以上的伤害/选择弃置两张以上的牌时，你将该事件的下限数值改为2/1/1（X为你的体力上限）",
					"ansory_selftest_robustness": "抗异",
					"ansory_selftest_robustness_info": "锁定技，所有其他角色使用牌的次数-X；当你受到一点伤害或失去一点体力时，你增加一点体力上限并摸X张牌；当你回复一点体力时，你失去一点体力上限并弃置X张牌（X为你失去的体力值+1）",
					// rEtc-OTH008 射箭器
					"ansory_selftest_shootarchery": "正击",
					"ansory_selftest_shootarchery_modify": "正击",
					"ansory_selftest_shootarchery_respond": "正击",
					"ansory_selftest_shootarchery_info": "锁定技，你使用【杀】无距离和次数限制；你使用杀时，你令此杀改为【万箭齐发】；其他角色响应你使用的万箭齐发时、或当一名其他角色因你使用的万箭齐发进入濒死并脱离濒死时，其摸一张牌，然后你展示并获得其区域内一张牌：若此牌不为基本牌，你令此万箭齐发对当前角色重新结算。",
					// rEtc-OTH009 膜法器
					"ansory_selftest_onlyself": "无他",
					"ansory_selftest_onlyself_info": "转化技，锁定技，阳：当你即将受到其他角色造成的伤害时，你将伤害来源改为你；阴：当你即将对一名其他角色造成伤害时，你将伤害来源改为其。",
					"ansory_selftest_onlyself": "轮还",
					"ansory_selftest_onlyself_info": "锁定技，当你使用牌指定目标时，你将此牌的使用者改为目标角色；若此时你拥有技能【无他】，你转换【无他】的状态。",
					// rEtc-OTH00A 使用器
					"ansory_selftest_translate": "转化",
					"ansory_selftest_translate_single": "转化",
					"ansory_selftest_translate_multi": "转化",
					"ansory_selftest_translate_info": "你可以将任意张牌当作一张基本牌或普通锦囊牌使用或打出。",
				},
			},
			intro: "杂项技能合集，用于存储代码并提供测试途径",
			author: "Ansory Solution",
			diskURL: "",
			forumURL: "",
			version: "0.0.0",
		}, files: { "character": [], "card": [], "skill": [] }
	}
});
